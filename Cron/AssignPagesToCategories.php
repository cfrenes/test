<?php

declare(strict_types=1);

namespace Tudock\Task\Cron;

use Magento\Catalog\Api\CategoryListInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;

class AssignPagesToCategories
{
    /**
     * @var CategoryListInterface
     */
    protected CategoryListInterface $categoryList;

    /**
     * @var SearchCriteriaBuilder
     */
    protected SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var PageRepositoryInterface
     */
    protected PageRepositoryInterface $pageRepository;

    /**
     * @var CategoryRepositoryInterface
     */
    protected CategoryRepositoryInterface $categoryRepository;

    /**
     * @param CategoryListInterface $categoryList
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param PageRepositoryInterface $pageRepository
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(
        CategoryListInterface       $categoryList,
        SearchCriteriaBuilder       $searchCriteriaBuilder,
        PageRepositoryInterface     $pageRepository,
        CategoryRepositoryInterface $categoryRepository
    )
    {
        $this->categoryList = $categoryList;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->pageRepository = $pageRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @throws LocalizedException
     */
    public function execute() : void
    {
        // get list of all categories with `prevent_automatic_update` to false (0)
        $searchCriteriaCategories = $this->searchCriteriaBuilder
            ->addFilter('prevent_automatic_update', 0)
            ->create();
        $categories = $this->categoryList->getList($searchCriteriaCategories)->getItems();

        // get list of all enabled CMS pages
        $searchCriteriaPages = $this->searchCriteriaBuilder
            ->addFilter('is_active', 1)
            ->create();
        $pages = $this->pageRepository->getList($searchCriteriaPages)->getItems();

        $pageIdentifier = [];

        // create a list of all cms page identifier as these are the values that are stored comma seperated
        // in the attribute `cms_page_list`
        foreach ($pages as $page) {
            $pageIdentifier[] = $page->getIdentifier();
        }

        foreach ($categories as $category) {
            // choose 4 CMS page identifiers
            $randPages = [];
            $randPageIdentifierKeys = array_rand($pageIdentifier, 4);

            foreach ($randPageIdentifierKeys AS $randPageIdentifierKey) {
                $randPages[] = $pageIdentifier[$randPageIdentifierKey];
            }

            $categoryModel = $this->categoryRepository->get($category->getId());
            $categoryModel->setCustomAttribute('cms_page_list', implode(',', $randPages));
            $this->categoryRepository->save($categoryModel);
        }
    }
}
