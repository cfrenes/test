<?php

declare(strict_types=1);

namespace Tudock\Task\Setup;

use Magento\Catalog\Model\Category;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\{
    ModuleContextInterface,
    ModuleDataSetupInterface,
    InstallDataInterface
};

use Magento\Eav\Setup\EavSetupFactory;

class InstallData implements InstallDataInterface
{
    private EavSetupFactory $eavSetupFactory;

    /**
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) : void
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $groupName = 'Content for Category';

        $eavSetup->addAttributeGroup(
            Category::ENTITY,
            $eavSetup->getDefaultAttributeSetId(Category::ENTITY),
            $groupName,
            1985
        );

        $eavSetup->addAttribute(Category::ENTITY, 'prevent_automatic_update', [
            'type'       => 'int',
            'label'      => 'Prevent Automatic Update',
            'input'      => 'boolean',
            'source'     => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
            'visible'    => true,
            'default'    => '0',
            'required'   => false,
            'global'     => ScopedAttributeInterface::SCOPE_GLOBAL,
            'group'      => $groupName,
            'sort_order' => 10
        ]);

        $eavSetup->addAttribute(Category::ENTITY, 'cms_page_list', [
            'type'       => 'text',
            'backend'    => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
            'label'      => 'CMS page list',
            'input'      => 'multiselect',
            'visible'    => true,
            'default'    => '',
            'required'   => false,
            'global'     => ScopedAttributeInterface::SCOPE_GLOBAL,
            'group'      => $groupName,
            'sort_order' => 20
        ]);
    }
}
